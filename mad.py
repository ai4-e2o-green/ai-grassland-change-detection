import otbApplication
import rasterio


def calculate_change_detection (reference_image_path, target_image_path, output_image_path):


    # Initialize OTB MAD application and configure the parameters
    app = otbApplication.Registry.CreateApplication("MultivariateAlterationDetector")

    app.SetParameterString("in1", reference_image_path)
    app.SetParameterString("in2", target_image_path)
    app.SetParameterString("out", output_image_path)

    try:
        app.ExecuteAndWriteOutput()
                
    except Exception as e:
        print (e)
        print ("Input parameters are not valid! Please update them and try again.")


calculate_change_detection("../data/cd_313_2019-03-22_2019-08-06_d81e044f.tiff", \
                            "../data/cd_313_2019-03-22_2020-04-05_b172aa49.tiff",\
                            "../data/test_cd.tif")