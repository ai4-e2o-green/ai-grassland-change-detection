# Change detection function based on MAD algorithm

## Usage

```
python3 -m venv venv
source venv/bin/activate
python mad.py
```

## Requirements

Python 3.6+\
OTB (https://www.orfeo-toolbox.org/CookBook/Installation.html)\
requirements.txt
